import os
import testinfra.utils.ansible_runner
import pwd

pkg_name='docker-ce'
svc_name='docker'
docker_user='docker'
docker_group='root'
docker_folder='/var/lib/docker'

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

def test_docker_directory_exists(host):
    docker_dir_exists = host.file("/var/lib/docker").exists
    assert docker_dir_exists

def docker_is_installed(host):
    docker = host.package(pkg_name)
    assert docker.is_installed

def docker_running_and_enabled(host):
    docker = host.service(svc_name)
    assert docker.is_running
    assert docker.is_enabled
    
def test_docker_user_exists(host):
    user_exists = False
    target_users = ['docker']
    for user_info in pwd.getpwall():
        if user_info.pw_name in target_users:
            user_exists = True
            break
    assert user_exists

    
